
print ()

# --------------------------------
# IMPORTS

import requests
import random
import json
import os

# Make sure dotenv module is installed
try:
	from dotenv import load_dotenv
# If not installed, throw an ImportError exception and offer advice
except ImportError as ie:
	print (ie)
	print ('Try: \'pip3 install python-dotenv\'\n')
	quit()
else:
	load_dotenv()

# Make sure discord module is installed
try:
	import discord
	from discord.ext import commands
except ImportError as ie:
	print (ie)
	print ('\n Try: \'pip3 install discord.py\'\n')
	quit()
# If installed, instantiate a bot
else:
	bot = commands.Bot(command_prefix='/')

# --------------------------------
# CONSTANTS

DISCORD_TOKEN = os.getenv('DISCORD_TOKEN')
TENOR_API_KEY = os.getenv('TENOR_API_KEY')
TENOR_GIF_LIMIT = 8
DEBUG = True

EMOJI_JOY = u"\U0001F602"

TERMINAL_COLORS = {
	"red": "\033[31;1m",
	"green": "\033[32;1m",
	"yellow": "\033[33;1m",
	"blue": "\033[34;1m",
	"magenta": "\033[35;1m",
	"cyan": "\033[36;1m",
	"lgray": "\033[37;1m",
	"dgray": "\033[90;1m",
	"lred": "\033[91;1m",
	"lgreen": "\033[92;1m",
	"lyellow": "\033[93;1m",
	"lblue": "\033[94;1m",
	"lmagenta": "\033[95;1m",
	"lcyan": "\033[96;1m"
}

# --------------------------------
# FUNCTIONS

def color_text(color, text):
	return (TERMINAL_COLORS[color] + text + '\033[0m')

# --------------------------------
# VALIDATION

# If we couldn't initialize an environmental variable,
# print what was looked for in friendly terms, then print what the program expected to find
def LocalEnvironmentVarNotFound(lookedFor, expecting):
	print (
		color_text('red', f'Could not find {lookedFor}.') + '\n'
		'Make sure there is a ' + color_text("dgray", ('\'' '.env' '\'')) + ' file in the root of the project, and that it contains the line:\n' +
		color_text('dgray', ('\'' + f'{expecting}' + '\'')) + '\n'
	)
	quit()

# Make sure discord token exists, otherwise give up
if DISCORD_TOKEN == None:
	LocalEnvironmentVarNotFound('Discord token', 'DISCORD_TOKEN=Put.Token.Here')

# Also make sure we have an API key for tenor
if TENOR_API_KEY == None:
	LocalEnvironmentVarNotFound('Tenor API KEY', 'TENOR_API_KEY=Put.API.KEY.Here')

# --------------------------------
# EVENTS

# Print a message when the bot is logged in and ready
@bot.event
async def on_ready():
	print(f'We have logged in as {bot.user}')

# Listen for messages in channels the bot is in, as well as direct messages
@bot.event
async def on_message(message):
	if DEBUG:
		# Print out messages to/from the bot for debugging
		print ('{0.author}: {0.content}'.format(message))
	else:
		# If not debugging, no need to check for the bot's own messages
		if message.author == bot.user:
			return

	# Get the context of the message.  Context is important
	ctx = await bot.get_context(message)

	# Get lowercase text of the message in a variable for cleanliness
	text = message.content.lower()

	# Mandatory response to ping
	if text.startswith('ping'):
		await ctx.send('pong')

	# Bot laughs at anything related to a fart
	if "fart" in text:
		await message.add_reaction(EMOJI_JOY)

	# For now, look for '/gif' as a message, rather than a command
	if text.startswith('/gif'):
		args = text.split()
		if len(args) != 2:
			await ctx.send('Usage: /gif search_term')
		else:
			# Use text after '/gif ' as the search term
			search_term = args[1]

			r = requests.get(
				"https://g.tenor.com/v1/random?q=%s&key=%s&limit=%s" % (search_term, TENOR_API_KEY, TENOR_GIF_LIMIT)
			)

			if r.status_code == 200:
				# return the gifs
				gifs = json.loads(r.content)

				# Print out the whole json blob because the documentation is ass
				#print ( json.dumps(gifs, indent=4) )
				# Now that I know what to actually expect from the response...

				# Get the url of a random gif
				gif = random.choice(gifs["results"])["media"][0]["gif"]["url"]

				print ( f'{bot.user}: ' + gif )
				await ctx.send(gif)
			else:
				# handle a possible error
				gifs = None

# ping command
@bot.command(pass_context=True)
async def ping(ctx):
	# Respond with pong
	await ctx.send("pong")

# --------------------------------
# FIN

# Start bot by logging in with token, then wait for events
bot.run(DISCORD_TOKEN)
